import React, {useEffect, useState} from "react";
import {Card, Divider, Grid, Input} from "semantic-ui-react";
import _ from "lodash";

export default function Search() {
    const [searchTerm, setSearchTerm] = useState('')
    const [searchResults, setSearchResults] = useState([])
    const handleChange = _.debounce((event) => {
        setSearchTerm(event.target.value)
    }, 300)
    useEffect(()=> {
        if(searchTerm.length > 1) {
            async function searchPlanets() {
                let res = await fetch(`https://swapi.dev/api/planets/?search=${searchTerm}`)
                let data = await res.json()
                setSearchResults(data.results)
                console.log(searchResults)
            }
            searchPlanets()
        }
    },[searchTerm])
    return (
        <div>
            <Input focus
                   placeholder="Search planets..."
                   onChange={handleChange}/>
            <Divider/>
            <Grid columns={3}>
                {searchResults && searchResults.map((planet, i)=> {
                    return (
                        <Grid.Column key={i}>
                            <Card>
                                <Card.Header>
                                    {planet.name}
                                </Card.Header>
                                <Card.Description>
                                    <strong>Climate</strong>
                                    <p>{planet.climate.toUpperCase()}</p>
                                    <strong>Diameter</strong>
                                    <p>{planet.diameter}</p>
                                    <strong>Population</strong>
                                    <p>{planet.population.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                                </Card.Description>
                            </Card>
                        </Grid.Column>
                    )
                })}

            </Grid>
        </div>
    )
}