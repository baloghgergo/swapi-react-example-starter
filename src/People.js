import React from "react";
import {Card, Grid} from "semantic-ui-react";

export default function People({data}) {
    return (
        <Grid columns={3}>
            {data && data.map((person, i) => {
                return (
                    <Grid.Column key={i}>
                        <Card>
                            <Card.Content>
                                <Card.Header>
                                    {person.name}
                                </Card.Header>
                                <Card.Description>
                                    <strong>Height</strong>
                                    <p>{person.height}</p>
                                    <strong>Weight</strong>
                                    <p>{person.mass} KG</p>
                                    <strong>Hair color</strong>
                                    <p>{person.hair_color}</p>
                                </Card.Description>
                            </Card.Content>
                        </Card>
                    </Grid.Column>
                )
            })}
        </Grid>
    )

}