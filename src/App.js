import './App.css';
import {useEffect, useState} from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import People from "./People";
import Navbar from "./Navbar";
import {Container} from "semantic-ui-react";
import Search from "./Search";

function App() {
    const [people, setPeople] = useState('');
    useEffect(() => {
        async function fetchPeople() {
            let res = await fetch('https://swapi.dev/api/people/')
            let data = await res.json()
            console.log(data)
            setPeople(data.results)
        }

        fetchPeople()

    }, [])
    return (
        <div className="App">
            <Router>
                <Navbar/>
                <Container>
                    <Switch>
                        <Route exact path='/'>
                            <People data={people}/>
                        </Route>
                        <Route exact path='/search'>
                            <Search/>
                        </Route>
                    </Switch>
                </Container>
            </Router>
        </div>
    );
}

export default App;
