import React from "react";
import {Container, Menu} from "semantic-ui-react";
import {Link} from "react-router-dom";

export default function Navbar() {
    return (
        <Menu inverted>
            <Container>
                <Link to='/'>
                    <Menu.Item name='Star wars people'/>
                </Link>
                <Link to='/search'>
                    <Menu.Item name='Search planets'/>
                </Link>
            </Container>
        </Menu>
    )
}